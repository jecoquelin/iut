#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Création d'un type chaine chaine20 de 20 caractères
typedef char chaine20[21];

// Création d'une structure t_region contenu le nom, la population et la capitale
typedef struct {
    chaine20 nom ;
    int population ;
    chaine20 capitale ;
} t_region ;


// Procédure permettant d'intialiser une structure de type t_region
void init(t_region *P1){
    printf("Entrer le nom de la region :\n");
    scanf("%s", &P1->nom);
    printf("Entrer la taille de sa population :\n");
    scanf("%d", &P1->population);
    printf("Entre la capital de cette region :\n");
    scanf("%s", &P1->capitale);
}

// Procédure permettant d'afficher les élements d'un type t_region
void afficher(t_region p1){
    printf("-- REGION --\nNom : %s\tPopulation : %d\tCapitale : %s\n", p1.nom, p1.population, p1.capitale);
}

// Programme principal
int main(){

    // Question 1
    // t_region P1 ;
    // init(&P1);
    // afficher(P1);

    // Question 2 
    t_region *pt_region ;
    pt_region = (t_region *)malloc(sizeof(t_region) *1);
    strcpy(pt_region->nom, "Bretagne") ;
    pt_region->population = 600000 ;
    strcpy(pt_region->capitale, "Rennes");
    //printf("%s", pt_region->capitale);

    // Question 3
    init(pt_region);
    afficher(*pt_region);
}