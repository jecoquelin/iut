#include <stdio.h>
#include <stdlib.h>

int main(){

    // Question 1 
    int x = 100 ;
    int *pt = &x ;

    printf("x : %d, *pt : %d\n", x, *pt);

    *pt = *pt + 10 ;
    printf("x : %d, *pt : %d\n", x, *pt);

    *pt = 100 ;
    printf("Valeur pointe : %d\n", *pt);


    // Question 2 
    int *pt2;
    pt2 = (int *)malloc(sizeof(int));
    *pt2 = 100;
    printf("Valeur pointe : %d\n", *pt2);
}