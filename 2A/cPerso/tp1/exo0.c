#include <stdio.h>
#include <stdlib.h>

int main(){
    int x = 10 ;
    int y = 30 ;
    int *ptr1 ;
    int *ptr2 ;

    printf("x : %d, y : %d\n", x, y);
    ptr1 = &x ;
    printf("x : %d, y : %d\n", x, y);
    y = *ptr1 ;
    printf("x : %d, y : %d\n", x, y);
    x = 50 ;
    printf("x : %d, y : %d\n", x, y);
    *ptr2 = *ptr1 ;
    printf("x : %d, y : %d\n", x, y);
}