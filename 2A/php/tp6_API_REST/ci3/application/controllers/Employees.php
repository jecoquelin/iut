<?php

    class Employees extends CI_Controller {
        public function __construct(){
            parent::__construct();
            $this->load->database();
            $this->load->helper('url');
        }

        public function index(){
            $tuples = $this->db->get("employee")->result();
            $data["status"] = 200 ;
            $data["message"] = $tuples ;
            return $this->respond($data);
        }

        public function respond($data){
            $status = $data["status"];
            $message = $data["message"];
            $this->output
                    ->set_status_header($status)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($message, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit ;
        }

        public function show($id=null){
            $tuples = $this->db->get_where("employee", ['id' => $id])->row_array();
            if ($tuples){
                $data["status"] = 200 ;
                $data["message"] = $tuples ;
                return $this->respond($data);
            }
            else {
                $message = array('success'=> 'No tuple found');
                $data["status"] = 200 ;
                $data["message"] = $message ;
                return $this->respond($data);
            }
        }

        // Permet de créer un nouveaux employée 
        // localhost:8080/index.php/Employees/create?id=3&name=kelian&email=keke@gail.com
        public function create(){
            $data = [
                'id' => $this->input->get('id'),
                'name' => $this->input->get('name'),
                'email' => $this->input->get('email')
            ];
            $this->db->insert('employee', $data);
            $messages = array('success'=>'Tuple created successfully');
            $data["status"]=201;
            $data["message"] = $messages ;
            return $this->respond($data);
        }

        // Permet de modifier un employée 
        // localhost:8080/index.php/Employees/update/1?name=Eude&email=eude@gmail.com
        public function update($id){
            $data = $this->db->get_where("employee", ['id' => $id])->row_array();
            if($data){
                $this->db->where('id', $id);
                $data = [
                    'name' => $this->input->get('name'),
                    'email' => $this->input->get('email')
                ];
                $this->db->update('employee', $data);
                $messages = array('success'=>'Tuple updated successfully');
                $data["status"]=200 ;
                $data["message"] = $messages ;
                return $this->respond($data);
            }
            else {
                $messages = array('success'=>'Tuple does not exist');
                $data["status"]=200;
                $data["message"]=$messages ;
                return $this->respond($data);
            }
        }

        // Permet de supprimer un employée
        // localhost:8080/index.php/EMployees/delete/3
        public function delete($id){
            $data = $this->db->get_where("employee", ['id'=> $id])->row_array();
            if ($data){
                $this->db->where('id', $id);
                $this->db->delete('employee');
                $messages = array('success'=>'Tuple deleted successfully');
                $data["status"]=200 ;
                $data["message"] = $messages ;
                return $this->respond($data);
            }
            else {
                $messages = array('success'=> 'Tuple does not exit');
                $data["status"]=200;
                $data["mesage"]=$messages ;
                return $this->respond($data);
            }
        }

        
    }

?>