<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo "$content"?> Tuto CodeIgniter 3</title>
</head>
<div id="global">
        <div id="entete">
            <h1> CodeIgniter 3 Tutorial </h1>
        </div><!-- # entete -->
        <div id="contenu">
            <?php $this->load->view($content);?>
        </div><!-- # contenu -->
        <div id="pied">
            <strong>&copy;2022</strong>
        </div><!--#pied-->
    </div><!--#global-->
</html>