<?php
    class Tasks extends CI_Controller {
        public function __construct(){
            parent::__construct();
            $this->load->model('tasks_model');
        }

        public function index(){
            // Charge les données
            $data['employee'] = $this->tasks_model->get_tasks();
            $data['title'] = 'Employers' ; // Donne un titre
            $data['content'] = 'employe_list'; // Appel de la vue employe_list
            
            $this->load->vars($data);
            $this->load->view('template');
        }


    }
?>