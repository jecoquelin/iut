<?php
    include('connect_params.php');
    try {
        // DSN
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // Permet d esupprimer les infos apparaissant 2 fois copies
        $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        
?>

<!Doctype html>
<html>
    <head>
        <title>Création de compte</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>Injection SQL</h1>
        <article>
            <h2>Exercice 1</h2>
            <form id="formuaire" method="post" action="main.php">
                <label>Identifiant </label><input type="text" name="id" required><br>
                <label>Mot de passe </label><input type="password" name="mdp"required><br>
                <label>Confirmer mot de passe </label><input type="password" name="mdpC"required><br>
                <input type="submit" value="Créer"/>
            </form>
        </article>
<?php
if (!empty($_POST['id']) and !empty($_POST['mdp']) and !empty($_POST['mdpC'])){
    if ( ($_POST['mdp'] != $_POST['mdpC']) ){
        echo "<p style='color : red ;'>Mot de passe différents</p>" ;
    }
    else {
        $q = $dbh->prepare("SELECT * FROM tp4._utilisateur WHERE id='".$_POST['id']."'");
        $q -> execute();
        $r = $q->fetchAll(\PDO::FETCH_ASSOC);
        if ( !empty($r) ){
            echo "utilisateur déjà pris !";
        }
        else {
            $q = "insert into tp4._utilisateur(id, mdp) values ('".$_POST['id']."','".$_POST['mdp']."')";
            $r = $dbh -> query($q);
            echo "Utilisateur ajouté !" ;
        }
    }
}
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}
?>        
        <article>
            <h2>Exercice 2</h2>
            <form id='connect' method="POST" action="result2.php">
                <label>Identifiant</label><input type="text" name="id"><br>
                <label>Mot de passe</label><input type="text" name="mdp"><br>
                <input type="submit" value="Se connecter">
            </form>
        </article>
    </body>
</html>