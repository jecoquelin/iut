<!DOCTYPE html>

<?php
    $nom = 'HAMARD' ;
    $prenom = 'Kélian' ;

    $tab = array('Professeur X', 'Angel', 'Beast', 'Cyclops', 'Marvel Girl', 'Iceman');
?>

<html>
    <head>
        <title>TP1 : Bootstrap</title>
        <meta charset="utf-8"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    </head>

    <body>
        <header class="bg-dark text-danger">
            <?php echo "<strong>Nom :</strong> $nom <strong>Prenom :</strong> $prenom" ; ?>

        </header> 
        
        <main class="p-center">
        
            <?php echo("<ol>");
            foreach($tab as $c => $v){
                echo("<li>$c : $v</li>");
            }
            echo("</ol>");
            ?>

        </main>

    </body>


</html>

