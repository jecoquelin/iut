<!DOCTYPE html>
<?php
    $super = array();

    $tab = array("super" => array('Cyclops', 'Beast', 'Marvel Girl'),
                 "nom" => array(array('summers', 'Mckoy'), array('Parker', 'McKoy', 'Braddock'), array('Grey', 'Xavier', 'Lebeau', 'Drake')), 
                 "Réponse" => array('Summers', 'McKoy', 'Grey')
);
/*  Permet d'afficher un élément précis du tableau
    $result = $tab["nom"][0][1];
    echo "$result";
*/
?>

<html>
    <head>
        <title>Questionnaire</title>
        <meta charset="utf-8"/>
    </head>

    <body>
        <main>
            <?php 
                foreach ($tab["super"] as $c => $v){
                    echo "<p>Quel est le nom de $v ?</p>";
                    echo "<ul>";
                    foreach ($tab['nom'][$c] as $c => $v){
                        echo "<li>$v</li>";
                    }
                    echo "</ul>" ;
                }
            ?> 
        </main>
    </body>
</html>