<?php 
    session_start();

    // Parti de l'exo 3
    if (!isset($_SESSION['color'])) {
        $_SESSION['color'] = '#ffffff' ;
    }
    elseif (isset($_GET['couleur'])) {
        $_SESSION['color'] = $_GET['couleur'] ;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Cookies et Sessions</title>
        <meta charset="utf-8"/>
        <style>
            html {background-color : <?php echo $_SESSION['color']; ?> ;}
        </style>
    </head>
    <body>
        <header>
            <h2>Cookies et Sessions</h2>
        </header>
        <body>
            <section>
                <article>
                    <h4>Exo 1</h4>
                    <?php include("exo1.php");?>
                </article>
                <article>
                    <h4>Exo 2</h4>
                    <?php include("form2.php"); include("result2.php");?>
                </article>
                <article>
                    <h4>Exo 3</h4>
                    <?php include("exo3.php");?>
                </article>
            </section>
        </body>
    </body>
</html>





