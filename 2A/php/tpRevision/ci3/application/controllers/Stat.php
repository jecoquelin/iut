<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Stat extends CI_Controller {
        public function display($content = 'home'){
            // Rechercje si la page 'home' existe
            if (!file_exists('application/views/'.$content.'.php')){
                show_404(); // Affiche l'erreur 404
            }
            $data['content'] = $content ;
            $this->load->vars($data); // Les données sont extraite et accès global à un acces global
            $this->load->view('template'); // Charge la page 'template'
        }
    }

?>