<?php
class Tasks extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('tasks_model');
        $this->load->helper('url');
    }     

    public function index(){
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['title']='List of tasks'; // a title to display above thelist
        $data['content']='main'; // template will call 'tasks_list ' sub -view
        $this->form_validation->set_rules('id','Id','required');
        $this->form_validation->set_rules('name','Name','required');
        if($this->form_validation->run()!==FALSE){
        $id=$this->input->post('id');
        $name=$this->input->post('name');
        $this->tasks_model->add_task($id,$name);
        }
        $data['tasks']=$this->tasks_model->get_tasks();
        $this->load->vars($data);
        $this->load->view('template');
        }
    
    public function create(){
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id','Id','required');
        $this->form_validation->set_rules('name','Name','required');
        if($this->form_validation->run()===FALSE){
            $data['content']='form';
        }
    else{
        $id=$this->input->post('id');
        $name=$this->input->post('name');
        $this->tasks_model->add_task($id,$name);
        $data['content']='add_success';
    }
    $data['tasks']=$this->tasks_model->get_tasks();
    $this->load->vars($data);
    $this->load->view('template');
    }

    // public function delete($id){
    //     $this->tasks_model->delete_task($id);
    //     $data['content']='del_success';
    //     $this->load->vars($data);
    //     $this->load->view('template');
    //     }

        public function delete($id){
            $this->tasks_model->delete_task($id);
            $this->index();
            }
 
}