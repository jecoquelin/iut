<?php
class Tasks_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get_tasks(){
        $query=$this->db->get('tp4._tache');
        return $query->result_array();
    }

    public function add_task($id,$name){
        $data=array(
        'id'=>$id,
        'name'=>$name// Argument given to the method
        );
        return $this->db->insert('tp4._tache',$data);
    }

    public function delete_task($id) {
        $data=array('id'=>$id);
        $this->db->delete('tp4._tache',$data);
        }
}
?>