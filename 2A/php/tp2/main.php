<!DOCTYPE html>
<html>
    <head>
        <title>Persistance des données</title>
        <meta charset="utf-8">
    </head>
    <body>
        <header>
            <h1>Persistance des données</h1>
        </header>
        <main>
            <section>
                <h2>Exercice</h2>
                <article>
                    <h3>Exo 1</h3>
                    <?php include("exo1.php") ;?>
                </article>
                <article>
                    <h3>Exo 2 : Case à cocher</h3>
                    <?php include("exo2.php") ;?>
                </article>
                <article>
                    <h3>Exo 2b : Liste à choix multiples</h3>
                    <?php include("exo2b.php") ;?>
                </article>
            </section>

            <section>
                <h2>Découverte +</h2>
                <article>
                    <h4>Nombre, curseur</h4>
                    <?php include("nombre_curseur.php")?>
                </article>
                <article>
                    <h4>Couleur</h4>
                    <?php include("couleur.php");?>
                </article>
                <article>
                    <h4>Date / Calendrier</h4>
                    <?php include("date.php");?>
                </article>
                <article>
                    <h4>Zone d'options : radio boutons</h4>
                    <?php include("radioBouton.php");?>
                </article>
                <action>
                    <p>Pour vérifier les compatibilités : <a href="https://caniuse.com/">caniuse</a> </p>
                </action>
            </section>

            <section>
                <h2>Exercice +</h2>
                <article>
                    <h4>Mini CRUD</h4>
                    <p><a href="miniCRUD.php">Lien</a></p>
                </article>
            </section>   

        </main>
    </body>
</html>