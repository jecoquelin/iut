<?php
    $super = array('Angel', 'Beast', 'Cyclops', 'Iceman');
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Formulaire</title>
        <meta charset="utf-8">
    </head>
    <body>
        <main>
            <form id="formulaire" method="get" action="result2.php">
                <?php
                foreach ($super as $c => $v) {
                    echo "<input type=\"checkbox\" name=\"$v\"><label>$v</label><br>";
                }
                ?>
                <input type="submit" value="Valider">
            </form>
        </main>
    </body>
</html>
