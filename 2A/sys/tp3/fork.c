#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
int main() {
    pid_t pid;
    int val = 10;
    printf("Avant fork(), je suis PID %d", getpid());
    printf(" et val = %d\n", val);
    pid = fork();
    //pid = -1 ;
    if (pid == -1){
        perror("Erreur fork ");
        return EXIT_FAILURE ;
    }
    else if (pid == 0) { // Fils 
        sleep(1);
        printf("F : Je suis le fils, mon PID est %d et mon pere a le PID + %d\n", getpid(), getppid());
        printf("F : Chez le fils, val = %d\n", val);
        val = 20;        
    } else { // Pere 
        printf("P : Je suis le pere, mon PID est %d et je viens de creer un fils de PID %d\n", getpid(), pid);
        printf("P : Chez le père, val = %d\n", val);
        // sleep(1);
    }
    printf("N : Je suis PID %d et val = %d\n", getpid(), val);
    wait();

    return EXIT_SUCCESS;

}