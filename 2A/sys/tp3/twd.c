#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>


void fini(){
    printf("L'enfant a fini !!\n");
    printf("%d", getpid());
    kill(getpid(), SIGTERM);
}

int main() {
    pid_t pid;
    pid = fork();

    signal(SIGCHLD, fini);

    if (pid == -1){
        perror("Erreur fork ");
        return EXIT_FAILURE ;
    }
    else if (pid == 0) { // Fils 
        printf("F : Je suis le fils, mon PID est %d et mon pere a le PID + %d\n", getpid(), getppid());      
        sleep(3);
    } else { // Pere 
        printf("P : Je suis le pere, mon PID est %d\n", getpid());
        int i = 0 ;
        while ( (i < 300) ){
            printf("P : Passage %d\n", i);
            i++ ;
            sleep(1);
        }
    }

    printf("N : Je suis PID %d \n", getpid());
    
    
    //wait();
    return EXIT_SUCCESS;

}