#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

int main(){
    // Création d'une clé à partir du dossier courant
    key_t cle ;
    cle = ftok(".", 0); 


    // Création du handler
    int handler ;
    handler = semget(cle, 1, IPC_CREAT | 0640); // Création d'un lot de 1 sem
    
    int ret ;
    ret = semctl(handler, 0, SETVAL, 1) ;

    /*
        Pour voir les semafore existant : ipcs

        Pour supprimer un semaphore : ipcrm -s <ID>

    */
}