#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdbool.h>


int main(){
    key_t cle;
    cle = ftok(".", 0);

    int handler ;
    handler = semget(cle, 1, 0640);

    int ret ;
    struct sembuf sem ;
    sem.sem_num = 0 ;
    sem.sem_flg = 0 ;

    int rep = 0 ;

    bool b = true ;
    while (b){
        printf("Avant conso\n");
        sem.sem_op = -1 ;
        ret = semop(handler, &sem, 1) ;
        printf("Après conso\n");
        printf("Prêt à libérer ?\n");
        scanf("%d", &rep);
        printf("Avant libération\n");
        sem.sem_op = +1 ;
        ret = semop(handler, &sem, 1) ;
        printf("Après libération\n");

        sleep(1);
    
    }
}