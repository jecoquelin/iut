#include <stdio.h>
#include <unistd.h>

#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdbool.h>

/*
    Semaphore A représente la quantité de workers disponible

    Smaphore B représente la quantité de tâche soumise par le master

*/

// Créer un semaphore avec une clé en paramètre
int creerSemaphore(int numCle){
    key_t   cle     = ftok(".", numCle) ;
    int     handler = semget(cle, 1, IPC_CREAT | 0640) ;
    return handler ;
}

int initSemaphore(int handler){
    int ret = semctl(handler, 0, SETVAL, 0) ;
    return ret ;
}


void fin(int signal){
    printf("Signal SIGINT reçu\n") ;
    kill(getpid(), SIGINT) ;
}


// MAIN
int main(){
    // Attrape le Ctrl +C pour tuer le programme
    signal(SIGINT, fin);

    // Initialisation du semaphore A (nombre de worker dispo)
    int SAh = creerSemaphore(0);
    int SBh = creerSemaphore(1);

    // Initialisation du semaphore B (nombre de tâche soumise)
    int SAr = initSemaphore(SAh);
    int SBr = initSemaphore(SBh);

    // Initialisation des sem 
    struct sembuf semA ;
    semA.sem_num = 0 ;
    semA.sem_flg = 0 ;

    struct sembuf semB ;
    semB.sem_num = 0 ;
    semB.sem_flg = 0 ;


    
    // Qaund le master distribu une tâche
    while (true){
        semA.sem_op = +1 ;
        ret = semop(SAh, &semA, 1) ;
        semB.sem_op = -1 ;
        ret = semop(SBh, &semB, 1) ;
    }
    

}

