#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>

int main(int argc, char *argv[], char *envp[]){
    int PID = atoi(argv[1]);
    printf("PID insérer : %d\n", PID);
    kill(PID, "SIGCONT");
}