#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


int main(){
    bool sysCallOK = FALSE ;
    
    // Un exemple d'appel système
    sysCallOK = FALSE;
    while (!sysCallOK) {
        if (un_appel_system(...params...) == -1) {
            if (errno == EINTR) {
                continue; // Interrompu, on recommence (on boucle) !
            } else {
    //... Traiter les autres cas d'erreur ou faire un perror()
                break; // ou exit ou ce qui est adapté à la situation
            }
        } else {
            sysCallOK = TRUE; // Ca s'est bien passé, sortie de boucle
            }
        }
    }
}