#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>


int main(){
    char car;
    int rP;
    int oP = open("./mon_tube", O_RDONLY);
    if (oP == -1){
        perror("Erreur ouverute");
        return EXIT_FAILURE ;
    }

    while (rP = read(oP, &car, 1)) {
        
        if ( (car >= 'a') && (car <= 'z') ){
            car = car - 'a' + 'A';
        }
        printf("%c", car);
    }
    printf("\n");


    close(oP) ;
    return EXIT_SUCCESS;
}