#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>

#include <signal.h>
#include <string.h>

void monSignal(){
    printf("\nSignal SIGPIPE reçu\n");
}

int main(int argc, char *argv[]){
    signal(SIGPIPE, monSignal);

    if (argc >= 2 ){
        int oP = open("./mon_pipe", O_WRONLY);
        if (oP == -1){
            perror("Erreur ouverute");
            return EXIT_FAILURE ;
        }
    
        int wP ;
        int cpt = 1 ;
        while (cpt < argc){
            
            wP = write(oP, argv[cpt], strlen(argv[cpt]));

            printf("Valeiur de write : %d\n", wP);
            perror("Message ");
            

            cpt++ ;
            sleep(1);
            //wP = write(oP, " ", 1);

        }
        close(oP);
    }

    return EXIT_SUCCESS ;
}