#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>


int main(int argc, char *argv[]){

    if (argc >= 2 ){
        int oP = open("./mon_tube", O_WRONLY);
        if (oP == -1){
            perror("Erreur ouverute");
            return EXIT_FAILURE ;
        }
    
        int wP ;
        int cpt = 1 ;
        while (cpt < argc){
            wP = write(oP, argv[cpt], strlen(argv[cpt]));
            if (wP == -1){
                perror("Erreur ecriture ");
                return EXIT_FAILURE ;
            }
            cpt++ ;
            wP = write(oP, " ", 1);
        }
        close(oP);
    }

    return EXIT_SUCCESS ;
}