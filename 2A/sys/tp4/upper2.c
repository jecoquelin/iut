#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>


int main(){
    
    int oP = open("./mon_pipe", O_RDONLY | O_TRUNC);
    if (oP == -1){
        perror("Erreur ouverture ");
        return EXIT_FAILURE ;
    }
    close(oP);

    return EXIT_SUCCESS ;
}