#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main(){
    int ret;

    // Défini le masque
    umask(0000);

    // Supprime le fichier nommé "Test"
    ret = rmdir("Test");

    if (ret == -1){
        perror("pb");
        return EXIT_FAILURE;
    }

    // Créer le dossir "Test" avec les droits pour user et group
    ret = mkdir("Test", 0770);
    if (ret == -1){
        if (errno == EEXIST){
            printf("Un dossier possède déjà ce nom !");
        }
        else {
            perror("pb");
        }
        
        return EXIT_FAILURE;
    }
    else {
        printf("Dossier créer avec succès !");
    }
    
    return EXIT_SUCCESS;
}