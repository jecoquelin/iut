#include <stdio.h>
#include <stdlib.h>



int main(int argc, char *argv[], char *envp[]){
    printf("-----NOMBRE D'ARGUMENT-----\n");
    printf("\tNombre d'arguments : %d\n", argc);

    printf("-----LISTES DES ARGUMENTS-----\n");
    for (int i = 0 ; i < argc ; i++){
        printf("\tValeur de l'argument %d : %s\n", i,argv[i]);
    }
    
    /*
    int j = 0 ;
    printf("-----LISTE DES VARIABLES D ENNVIRONNEMENT-----\n");
    while (*envp[j]){
        printf("Var Env %d : %s\n", j, envp[j]);
        j++ ;
    }
    */
}