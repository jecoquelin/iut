#include <stdio.h>
#include <stdlib.h>
// Librérie pour open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
// Librérie pour write et read
#include <unistd.h>
// Pour récupérer les erreurs
#include <errno.h>

int main(){

    // J'ouvre le fichier "emplacement" de OLD
    int oOld = open("./old.txt", O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
    if (oOld == -1){
        perror("open old ");
    }
    else {
        printf("Fichier old ouvert en écriture\n");
    }
    // J'ouvre le fichier ORIG
    int oOrig = open("./ORIG.txt", O_RDONLY, S_IRWXU);
    if (oOrig == -1){
        perror("open orig");
    }
    else {
        printf("Fichier ORIG ouvert à la lecture\n");
    }
    // Je lis le fichier ORIG et stocke sont contenu
    char chaine[100];
    int rOrig = read(oOrig, chaine, 100);
    if (rOrig == -1){
        perror("Read Error Orig ");
    }
    else {
        printf("Lecture du contenu de ORIG\n");
        printf("Le fichier est de taille %d\n", rOrig);
    }

    // J'écrit dans le fichier old le contenu de orig
    int wOld = write(oOld, chaine, rOrig);
    if (wOld == -1){
        perror("Write Error Old ") ;
    }
    else {
        printf("Ecriture du contenu de ORIG dans old\n");
    }
 
}