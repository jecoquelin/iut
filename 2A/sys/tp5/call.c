#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>


int main(){
    int oC, rd, oL2C, oC2L, wC2L;
    char etage;

    oC2L = open("call2lift", O_WRONLY);
    oL2C = open("lift2call", O_RDONLY);

    /*********************
     * LECTURE CALL_PIPE *
     *********************/
    oC = open("call_pipe", O_RDONLY);
    if (oC == -1 ){
        perror("Ouverture call_pipe : ");
    }

    rd = read(oC, &etage, 1);
    if (rd == -1){
        perror("Lecture call_pipe : ");
    }
    printf("%c\n", etage);
    close(oC) ;

    /**********************
     * ECRITURE CALL2LIFT *
     **********************/
    // [0-5] -> lift
    if ( (etage>=0) and (etage <=5) ){
        wC2L = write(oC2L, etage, strlen(etage)) ;
        if (wC2L == -1){
            perror("Ecriture dans call2lift : ");
        }
    }
    else {
        print("Etage inconu, il doit être compris entre 0 et 5");
    }


    /*********************
     * LECTURE LIFT2CALL *
     *********************/
    // # <- lift


/*
    // Ecrit dans le tube call2lift

    close(oC2L);

    char car = '\\';

    while (car != '#'){
        read(oL2C, car, 1);
    }
    */
}