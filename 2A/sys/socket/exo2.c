#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h> // Socket
#include <arpa/inet.h> // Bind

int main(){
    // Fonction socket() - Client et Serveur
    printf("Création du socket !\n");
    int sock ;
    sock = socket(AF_INET, SOCK_STREAM, 0); // Retourne un handler : <=> fd
                                            // AF_INET : IpV4 internet protocol, SOCK_STREAM : 

    // Fonction bind() - Serveur seulement
    printf("Configuration des ports et du réseau : bind !\n");
    int ret ;
    struct sockaddr_in addr ;
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    addr.sin_family = AF_INET ;
    addr.sin_port = htons(7070);
    ret = bind(sock, (struct sockaddr *)&addr, sizeof(addr));


    // Fonction listen() - Serveur seulement
    printf("Le serveur se prépare à écouter que quelqu'un toque chez lui !\n");
    ret = listen(sock, 1);

    // Fonction accept() - Serveur seulement
    printf("Le serveur se prépare à accepter et faire attendre !\n");
    int size ;
    int cnx ;
    struct sockaddr_in conn_addr ;
    size = sizeof(conn_addr) ;
    cnx = accept(sock, (struct sockaddr *)&conn_addr, (socklen_t *)&size);

    // Exercice 2
    char car ;
    int rBlabla = recv(cnx, &car, sizeof(car), MSG_CMSG_CLOEXEC | MSG_TRUNC);
    if (rBlabla == -1){
        perror("Erreur lecture");
    }
    else {
        printf("%s", chaine);
    }
}
