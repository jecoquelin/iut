#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h> // Socket
#include <arpa/inet.h> // Bind

const int MAX = 100 ;

int main(){
    char chaine[MAX] ;
    char newChaine[MAX] ;
    int i = 0 ;
    int j = 0 ;
    int lec ;

    // Fonction socket() - Client et Serveur
    printf("Création du socket !\n");
    int sock ;
    sock = socket(AF_INET, SOCK_STREAM, 0); // Retourne un handler : <=> fd
                                            // AF_INET : IpV4 internet protocol, SOCK_STREAM : 

    // Fonction bind() - Serveur seulement
    printf("Configuration des ports et du réseau : bind !\n");
    int ret ;
    struct sockaddr_in addr ;
    addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // Localhost
    addr.sin_family = AF_INET ;
    addr.sin_port = htons(8081);
    ret = bind(sock, (struct sockaddr *)&addr, sizeof(addr));


    // Fonction listen() - Serveur seulement
    printf("Le serveur se prépare à écouter que quelqu'un toque chez lui !\n");
    ret = listen(sock, 1);

    // Fonction accept() - Serveur seulement
    printf("Le serveur se prépare à accepter et faire attendre !\n");
    int size ;
    int cnx ;
    struct sockaddr_in conn_addr ;
    size = sizeof(conn_addr) ;
    cnx = accept(sock, (struct sockaddr *)&conn_addr, (socklen_t *)&size);

    printf("Connexion établie !\n");

    lec = read(cnx, chaine, MAX) ;
    while ( lec > 0 ){
        printf("Message %d\n", i);

        j=0 ;
        strcpy(newChaine, "                                                        ") ;
        printf("\t");
        while ( (chaine[j] != '\r') && (j < MAX)){
            printf("%c", chaine[j]) ;
            newChaine[j] =  chaine[j];
            j++ ;
        }
        printf("\n\t%s\n", newChaine);
        lec = read(cnx, chaine, MAX) ;
        i++ ;
    }


        
}
