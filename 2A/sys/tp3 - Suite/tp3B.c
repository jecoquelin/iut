#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>

#include <sys/types.h>
#include <unistd.h>


bool continuer = true ;
bool demandeMort = false ;


void finFils(){
    continuer = false ;
}

void finProgramme(){
    printf("coucou !!\n");
    demandeMort = true ;
}


int main(int agrc, char * argv[]){
    int pid ;
    int i = 0 ;
    signal(SIGCHLD, finFils);
    signal(SIGINT, finProgramme);

    while (continuer){
        pid = fork();
        if (pid == 0) {
            printf("PID fils : %d\n", getpid());
            if (demandeMort){
                kill(SIGTERM, getpid());
            }
            else {
                int jsp = execl("/usr/bin/xeyes", "xeyes", "-fg", "red");
                if (jsp == -1){
                    perror("Erreur execl ");
                }
            }
        }
        else {
            printf("PID pere : %d\n", getpid());
            printf("Passage num %d\n", i);
            i++ ; 
            if (demandeMort){
                kill(SIGKILL, getpid());
            }
        }
        sleep(10) ;
    }
        

    return EXIT_SUCCESS ;
}