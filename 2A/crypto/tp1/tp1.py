#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Fonctions préliminaires
# =============================================================================
def convert_ASCII(texte):
    tabAscii = []
    for lettre in texte:
        tabAscii.append(ord(lettre))
    return tabAscii
#print(convert_ASCII("Bonjour"))

def convert_CHAR(textAscii):
    tab = []
    for lettre in textAscii :
        tab.append(chr(lettre))
    return tab
#bonjour = convert_ASCII("Bonjour")
#print(convert_CHAR(bonjour))


# =============================================================================
# Chiffrement de César
# =============================================================================
def chiffre_Cesar(texte, key):
    texte = convert_ASCII(texte)
    texteCode = []
    print(texteCode)
    for lettre in texte :
        texteCode.append( (lettre+key) %128)
    texteCode = convert_CHAR(texteCode)
    return texteCode
#texte = "Info"
#print(chiffre_Cesar(texte, 2))

def dechiffre_Cesar(texte, key):
    texte = convert_ASCII(texte)
    texteDecode = []
    for i in texte :
        texteDecode.append((i-key) %128)
    texteDecode = convert_CHAR(texteDecode)
    return texteDecode
#texte = chiffre_Cesar("Bonjour", 5)
#print(dechiffre_Cesar(texte, 5))

def frequence(texte):
    dic = {}
    texte = convert_ASCII(texte)
    for lettre in texte:
        if (lettre not in dic):
            dic.update({lettre:texte.count(lettre)})
    return dic
#print(frequence("anticonstitutionnel"))

def frequenceMax(dic):
    maxi = 0
    lettreM = ''
    for lettre in dic :
        if (maxi < dic[lettre]):
            maxi = dic[lettre]
            lettreM = lettre
    return lettreM
#bdicFrequence = frequence("anticonstitutionnellement")
#print(f'La lettre qui revient le plus souvent est : {chr(frequenceMax(dicFrequence))}')

cipher = "[ol'yhiip{4ovsl'~lu{'z{yhpno{'vu'sprl'h'{|uuls'mvy'zvtl'~h\x803'huk'{olu'kpwwlk'z|kklus\x80'kv~u3'zv'z|kklus\x803'{oh{'Hspjl'ohk'uv{'h'tvtlu{'{v'{opur'hiv|{'z{vwwpun'olyzlsm3'ilmvyl'zol'mv|uk'olyzlsm'mhsspun'kv~u'~oh{'zlltlk'h'kllw'~lss5'Lp{oly'{ol'~lss'~hz'}ly\x80'kllw3'vy'zol'mlss'}ly\x80'zsv~s\x803'mvy'zol'ohk'wslu{\x80'vm'{ptl'hz'zol'~lu{'kv~u'{v'svvr'hiv|{'oly3'huk'{v'~vukly'~oh{'~v|sk'ohwwlu'ul\x7f{5'Mpyz{3'zol'{yplk'{v'svvr'kv~u'huk'thrl'v|{'~oh{'zol'~hz'jvtpun'{v3'i|{'p{'~hz'{vv'khyr'{v'zll'hu\x80{opunA'{olu3'zol'svvrlk'h{'{ol'zpklz'vm'{ol'~lss3'huk'uv{pjlk'{oh{'{ol\x80'~lyl'mpsslk'~p{o'j|wivhykz'huk'ivvr4zols}lzB'olyl'huk'{olyl'~lyl'thwz'huk'wpj{|ylz'o|un'vu'wlnz5'Zol'{vvr'h'qhy'kv~u'vmm'vul'vm'{ol'zols}lz'hz'zol'whzzlkA'p{'~hz'shilsslk')Vyhunl'Thythshkl3)'i|{'{v'oly'nylh{'kpzhwwvpu{tlu{'p{'~hz'ltw{\x80A'zol'kpk'uv{'sprl'{v'kyvw'{ol'qhy3'mvy'mlhy'vm'rpsspun'zvtlivk\x80'|uklyulh{o3'zv'thuhnlk'{v'w|{'p{'pu{v'vul'vm'{ol'j|wivhykz'hz'zol'mlss'whz{'p{5)^lss()'{ov|no{'Hspjl'{v'olyzlsm3')hm{ly'z|jo'h'mhss'hz'{opz3'P'zohss'{opur'uv{opun'vm'{|tispun'kv~u'z{hpyz('Ov~'iyh}l'{ol\x80.ss'hss'{opur'tl'h{'ovtl('^o\x803'P'~v|sku.{'zh\x80'hu\x80{opun'hiv|{'p{3'l}lu'pm'P'mlss'vmm'{ol'{vw'vm'{ol'ov|zl()'/~opjo'~hz'tvz{'sprls\x80'{y|l505'Kv~u3'kv~u3'kv~u5'^v|sk'{ol'mhss'ul}ly'jvtl'{v'hu'lukF')P'~vukly'ov~'thu\x80'tpslz'P.}l'mhsslu'i\x80'{opz'{ptlF)'zhpk'zol'hsv|k3')P't|z{'il'nl{{pun'zvtl~olyl'ulhy'{ol'jlu{yl'vm'{ol'lhy{o5'Sl{'tl'zllA'{oh{'~v|sk'il'mv|y'{ov|zhuk'tpslz'kv~u3'P'{opur\xe9\x87\x9b)'/mvy'\x80v|'zll'Hspjl'ohk'slhyu{'zl}lyhs'{opunz'vm'{opz'zvy{'pu'oly'slzzvuz'pu'{ol'zjovvsyvvt3'huk'{ov|no'{opz'~hz'uv{'h'}ly\x80'nvvk'vwwvy{|up{\x80'vm'zov~pun'vmm'oly'ruv~slknl3'hz'{olyl'~hz'uv'vul'{v'olhy'oly3'z{pss'p{'~hz'nvvk'wyhj{pjl'{v'zh\x80'p{'v}ly30')\x80lz3'{oh{.z'{ol'ypno{'kpz{hujl3'i|{'{olu'P'~vukly'~oh{'Svunp{|kl'vy'Sh{p{|kl4spul'zohss'P'il'puF)'/Hspjl'ohk'uv'pklh'~oh{'Svunp{|kl'~hz3'vy'Sh{p{|kl'lp{oly3'i|{'zol'{ov|no{'{ol\x80'~lyl'upjl'nyhuk'~vykz'{v'zh\x8050"

def cryptanalyse(texte):
    f = frequence(texte)
    fM = frequenceMax(f)
    key = fM - 69
    print(key)
    texteDecode = dechiffre_Cesar(texte, key)
    return texteDecode

texte = "Bonjour je me presente je suis emmanuel"
#print(cryptanalyse("FdpMf"))
#print(cryptanalyse(texte))
