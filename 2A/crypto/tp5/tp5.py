#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 15:51:58 2022

@author: khamard
"""

# =============================================================================
# ALGORITHME DE LUHN
# =============================================================================

# src : https://www.ibm.com/docs/fr/order-management-sw/9.3.0?topic=cpms-handling-credit-cards

def typeCB(numCB):
    nom = ''
    if ( (numCB[0] == '5') and (numCB[1] in ['1','2','3','4','5']) and (len(numCB) == 16)):
        nom= 'MasterCard'
    elif ( (numCB[0] == '3') and  (numCB[1] in ['4','5','6','7']) and (len(numCB) == 15)) :
        nom = 'American Express'
    elif ( (numCB[0] == '4') and ( (len(numCB) == 13) or (len(numCB) == 16) ) ):
        nom = 'Visa'
    else :
        nom = 'non reconnu'

    return nom

def luhn():
    print("Veiller entrer votre n° de carte bleu")
    numCB = input() ;
          
    size = len(numCB)
    i = size - 2
    total = 0
    totalR = 0
    # Parcour du numero de CB plus additions
    while (i >= 0):
        nb = str(int(numCB[i])*2)
        j = 0
        while (j < len(nb)):
            total = total + int(nb[j])
            j = j + 1
        i = i - 2
    
    i=1
    while (i <= len(numCB)-1):
        totalR = totalR + int(numCB[i])
        i = i + 2
        
    totaux = total + totalR
    if (totaux % 10 == 0):
        print("Carte Bancaire valide !")
        print(f"C'est une {typeCB(numCB)}")
    else :
        print("Carte Bancaire invalide !")

# =============================================================================
# JEUX DE TEST
# =============================================================================
# luhn()
# Carte invalide : 4624748233249080 
# Carte valide : 4624748233249780


# =============================================================================
# =============================================================================

# =============================================================================
# CALCUL DE LA CLEF D'UN NUMERO DE SIREN
# =============================================================================

def calcul_cle(numS):
    
    total = 0
    i = len(numS) - 1
    while (i >= 0) :
        if ( (i%2) == 1):
            total = total + int(numS[i])
        elif (i % 2 == 0):
            total = total + int(numS[i])*2
        i = i - 1
            
    if (total % 10 == 0):
        numCrypt = 0
        #print(f"Siren good direct | clé de cryptage : {numCrypt}")
    else :
        numCrypt = 10 - (total%10)
        #print(f"Siren pas good direct | clé de cryptage : {numCrypt}")
    
    return (total+numCrypt), numCrypt

def siren():
    print("Veillez donner les 8 premier chiffres :")
    numSIREN8p = input()
    
    total, numCrypt = calcul_cle(numSIREN8p)
    
    print(f"La clé de cryptage est {numCrypt}")
    print(f"Le reste de la division du numero de SIREN avec la clé de crypatage par 10 est : {total%10}")
# =============================================================================
#   JEUX DE TEST
# =============================================================================
# 13928237
# siren()
    
    
    
    

