#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 15:53:50 2022

@author: khamard
"""

# =============================================================================
# TEST DE PRIMALITE : A LA RECHERCHE DE GRANDS NOMBRES PREMIERS
# =============================================================================


# =============================================================================
# TROUVER UN NOMBRE PSEUDO-PREMIER
# =============================================================================
# -- Expression affine-Dirichlet --
def Dirichlet(N):
    tab = []
    for i in range (0, N):
        tab.append(6*i+1)
    return tab

def Dirichlet2(p, N):
    tab = []
    for i in range(0, N):
        tab.append(30*i+p)
    return tab

print("Dirochlet : ", Dirichlet(9))
print("Dirochlet2 : ", Dirichlet2(1, 9))

# -- Polynôme du second degré --
def Euler(N):
    tab = []
    for i in range (0, N):
        tab.append(i**2 + i + 41)
    return tab

def Legendre(N):
    tab = []
    for i in range (0, N):
        tab.append(2*i**2 + 29)
    return tab
    
def Ruby(N):
    tab = []
    for i in range (0, N):
        tab.append(103*i**2 - 3945*i + 3431)
    return tab

#print("Euler : ", Euler(5))
#print("Legendre : ", Legendre(5))
#print("Ruby : ", Ruby(5))


# =============================================================================
# TEST DE FERMAT
# =============================================================================
# -- 1 -- 

def Fermat(p):
    
    if ( (2**(p-1))%p == 1):
        if ( (3**(p-1))%p == 1): 
            if ( (5**(p-1))%p == 1):
                if ( (7**(p-1))%p == 1):
                    return True, p
                else :
                    return False, p
            else :
                return False, p
        else :
            return False, p
    else :
        return False, p
        

def pourcentage(chaine):
    cpt = 0
    nb = 0
    prem = 0
    MAX = 100000
    if ("Euler" == chaine):
        nbT = Euler(MAX)
    if ("Legendre" == chaine):
        nbT = Legendre(MAX)
    if ("Ruby" == chaine):
        nbT = Ruby(MAX)
        
    while (nb < MAX):
        if (nbT[nb] > 10):
            B, nb = Fermat(nbT[nb])
            if (B == True):
                prem = prem + 1    
        cpt = cpt + 1
            
    return (prem / cpt ) * 100 
            
#print(f"Dans l'algorithme de Euler, nous avons {pourcentage('Euler')} % de nombre premier")     
#print(f"Dans l'algorithme de Legendre, nous avons {pourcentage('Legendre')} % de nombre premier")     
print(f"Dans l'algorithme de Ruby, nous avons {pourcentage('Ruby')} % de nombre premier")     


