#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 08:07:08 2022

@author: khamard
"""
def convert_ASCII(texte):
    tabAscii = []
    for lettre in texte:
        tabAscii.append(ord(lettre))
    return tabAscii
#print(convert_ASCII("Bonjour"))

def convert_CHAR(textAscii):
    tab = []
    for lettre in textAscii :
        tab.append(chr(lettre))
    return tab
#bonjour = convert_ASCII("Bonjour")
#print(convert_CHAR(bonjour))


# =============================================================================
# CHIFFRE DE VIGENERE
# =============================================================================
def chiffreVigenere(texte, key) :
    tCrypt = []
    cpt = 0
    tailleC = len(key)
    
    for lettre in texte :
        ecart = ord(key[cpt]) # Peut-être ajout de -1
        
        # Ajoute espace dans le cas où cela est un espace     
        if (ord(lettre) == 32) :
            tCrypt.append(chr(32))
            
        # Ajoute si ce n'est pas un espace
        if (ord(lettre) != 32) :
            letCryp = ord(lettre) + ecart
            if (letCryp > 122 ):
                letCryp = (letCryp % 123)
                #print(f'ascii : {letCryp}, chr : {chr(letCryp)}' )
            tCrypt.append(chr(letCryp))
            cpt += 1
            if (cpt >= tailleC):
                cpt = 0
    return tCrypt

texte = "Bonjour, je suis kelian"
cle = "icn"
texteCrypt = chiffreVigenere(texte, cle)
#print(texteCrypt)

# -- Deux nombre sont premier entre eux
# -- Methode d'euclide
def pgcd(nb1, nb2):
    if (nb1 == 0):
        return nb2
    if (nb2 == 0 ):
        return nb1
    if (nb1 == nb2):
        return nb1
    
    if (nb1 < nb2 ):
        return pgcd(nb1, nb2-nb1)
    elif (nb1 > nb2):
        return pgcd(nb1-nb2, nb2)    
#print(pgcd(994853,785))


def pgcdDistancesEntreRepetitions(tc, i):
    j = 0
    n = len(tc)
    tab = []
    
    while (j <= (n-3) ) :
        if (i != j):
            if ( (tc[i] == tc[j])  and (tc[i+1] == tc[j+1]) and (tc[i+2] == tc[j+2])) :
                tab.append(abs(j-i))
        j = j + 1
        
    if (len(tab)== 0):
        return 0
    elif (len(tab) == 1) :
        return tab[0]
    else :
        while (len(tab) >= 2) :
            nb1 = tab.pop()
            nb2 = tab.pop()
            print(nb1, nb2, pgcd(nb1, nb2))
            
            tab.append(pgcd(nb1, nb2))
    return tab[0]

tc1 = "KQOWE FVJPU JUUNU KGLME KJINM WUXFQ MKJBG WRLFN FGHUD WUUMB SVLPS NCMUE KQCTE SWREE KOYSS IWCTU AXYOT APXPL WPNTC GOJBG FQHTD WXIZA YGFFN SXCSE YNCTS SPNTU JNYTG GWZGR WUUNE JUUQE APYME KQHUI DUXFP GUYTS MTFFS HNUOC ZGMRU WEYTR GKMEE DCTVR ECFBD JQCUS WVBPN LGOYL SKMTE FVJJT WWMFM WPNME MTMHR SPXFS SKFFS TNUOC ZGMDO EOYEE KCPJR GPMUR SKHFR SEIUE VGOYC WXIZA YGOSA ANYDO EOYJL WUNHA MEBFE LXYVL WNOJN SIOFR WUCCE SWKVI DGMUC GOCRU WGNMA AFFVN SIUDE KQHCE UCPFC MPVSU DGAVE MNYMA MVLFM AOYFN TQCUA FVFJN XKLNE IWCWO DCCUL WRIFT WGMUS WOVMA TNYBU HTCOC WFYTN MGYTQ MKBBN LGFBT WOJFT WGNTE JKNEE DCLDH WTVBU VGFBI JG"  
tc2 = "XAUNM EESYI EDTLL FGSNB WQUFX PQTYO RUTYI INUMQ IEULS MFAFX GUTYB XXAGB HMIFI IMUMQ IDEKR IFRIR ZQUHI ENOOO IGRML YETYO VQRYS IXEOK IYPYO IGRFB WPIYR BQURJ IYEMJ IGRYK XYACP PQSPB VESIR ZQRUF REDYJ IGRYK XBLOP JARNP UGEFB WMILX MZSMZ YXPNB PUMYZ MEEFB UGENL RDEPB JXONQ EZTMB WOEFI IPAHP PQBFL GDEMF WFAHQ"
print(f'Taille : {len(tc1)}')
pgcd1 = pgcdDistancesEntreRepetitions(tc1, 5)
print(f'Le pgcd des distance entre les différents motif est : {pgcd1}')    
#pgcd2 = pgcdDistancesEntreRepetitions(tc2, 1)
#print(f'Le pgcd des distance entre les différents motif est : {pgcd2}') 

#def longueurCle(tc):    
#    return 

def extraction(tc, i):
    tabM = []
    for lettre in tc :
        tabM.append(lettre*i)
    return tabM

def trouveCle(tc):
    tailleMotifProbable = pgcdDistancesEntreRepetitions(tc, 5)
    
    for i in 





  