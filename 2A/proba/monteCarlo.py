#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 13:38:28 2022

@author: khamard
"""

"""
1-  Carré = 1² 
    Cercle = (Pi * 1² ) /4 = 0.785 ou Pi/4
    La probabilité que le point soit dans le cercle est de 78,5%

"""

import numpy as np
import math

# Génère 100 nombre aléatoire compris entre 0 et 1
# print(np.random.rand(100));

# =============================================================================
# Question 3 : APPROXIAMTION DE PI
# =============================================================================
def approxPi(n):
    cpt = 0
    cptHache = 0
    
    for i in range(n):
        x = np.random.rand(1);
        y = np.random.rand(1);
        if (math.sqrt(x**2 + y**2) <= 1):
            cptHache += 1 
        cpt += 1 
        
    return ( cptHache / cpt ) * 4

# Approximation du nombre Pi avec n
n = 10000
print(f'La valeur arrondi de Pi pour {n} lancé est de : {approxPi(n)}')


# =============================================================================
# QUESTION 5 : APPROXIMATION INTEGRAL DE EXP
# =============================================================================
def approxExp(n):
    cpt = 0
    cptHache = 0
    
    for i in range(n):
        x = np.random.rand(1);
        y = np.random.rand(1) * math.exp(1);
        if ( y <= math.exp(x) ):
            cptHache += 1 
        cpt += 1 
        
    return ( cptHache / cpt ) * math.exp(1)

# Approximation de l'intégral de 0 à 1 de exp(x)
n = 10000
print(f'L\'intégral de exp(x) entre 0 et 1 est de : {approxExp(n)}')


# =============================================================================
# QUESTION 6 : APPROXIMATION INTEGRAL GENERAL
# =============================================================================
def approxInt(n, a, b):
    cpt = 0
    cptHache = 0
    
    for i in range(n):
        t = np.random.rand(1) 
        x = a + t*(b-a)
        
        y = np.random.rand(1) * (2*x)+3;
        if ( y <= (a*x+b) ):
            cptHache += 1 
        cpt += 1 
        
    return ( cptHache / cpt ) * (a+b)

# Approximation de l'intégral entre a et b d'une intégral
n = 1000
print(f'L\'intégral de ax+b entre 0 et 1 est de : {approxInt(n, 5, 7)}')
















