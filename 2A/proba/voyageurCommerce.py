#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 10:27:22 2023

@author: khamard
"""

# =============================================================================
# LIBRAIRIE
# =============================================================================
import numpy as np 

# =============================================================================
# FONCTIONS
# =============================================================================

# n est le nombre de ville à visiter
def Carto(n=5, min=1, max=10) :
    # Initialisation de la carte à 0 avec une matrice carré n
    D = np.zeros((n,n))
    
    # Génération des distances des différentes villes
    for i in range (n) :
        for j in range (i) :
            if (i != j) :    
                D[i][j] = np.random.randint(min, max)
                D[j][i] = D[i][j] 
    
    # Retourne la matrice carré avec les distances
    return D

# Renvoie la population de départ
# n nombre de ville, m nb de solution paires 
def Populat(n=5, m=6):
    P = np.random.randint(2, n+1, size=(n, m))
    P[0] = 1
    
    return P
    
# Retourne la distance totale si l'on suit ce trajet
# s Une solution de P, D la carte des distance
def CalculAdapt(s = [1,2,4,3], D=Carto()):
    distanceTotale = 0
    size = np.size(s)-1
    
    for i in range (0, size) :
        distanceTotale += D[s[i]-1][s[i+1]-1]
    distanceTotale += D[s[-1]-1][0]
    
    return distanceTotale

# Renvoie les m meilleur solutions en comarant tout le monde(elle vire la moitié)
def SelectElit(P=Populat(), C = Carto(), m=6):
    #print(f'Populace :\n {P}\n')
    #print(f'Carte :\n {C}\n')
    # Il faudra le faire varier pour obtenir tous les chemins 0 = x
    r, c = np.shape(P)
    
    # On récupère les distances de tous les trajets
    listeDistance = {}
    for i in range (c):
        dist = CalculAdapt(P[:,i], C)
        listeDistance[i] = dist
   
    # On décide de garder la moitié des solutions (les plus courtes)
    nbElite = c / 2
    listeElite = np.zeros( (r, 1) )
    # Tant qu'on a pas récupéré la moitié des meilleurs solution, on récupère
    while nbElite > 0 :
        bestV = float('inf') # Initialisation du plus court chemin à +inf
        
        # Pour chaque Distance encore présent dans la liste on récupère sa clé et sa valeur
        for i in listeDistance:
            if (bestV > listeDistance[i]):
                bestK = i
                bestV = listeDistance[i]
        
        # On met la ligne ayant le plus court chemin dans la listeElite et on la retire de listeDistance
        listeElite = np.c_[listeElite, P[:, bestK]]
        #print(f'Meilleur chemin : {bestK} ==> {P[:, bestK]} ==> {listeDistance[bestK]}')
        listeDistance.pop(bestK)
          
        nbElite -= 1
        
    # On retire la colonne de zéros
    listeElite = np.delete(listeElite, 0, axis = 1)
    
    return listeElite
   
 
# Renvoie les m meilleur solutions (la moitié) en prenant deux chemins à chaque fois et garder q'une seul sur les deux à répéter m fois
def SelectTourn(P=Populat(), C=Carto(), m=6):
    listeTourn = []
    
    nbTourn = m/2
    
    while ( len(P[0, :]) > nbTourn):
        #print(P[0, :])
        # Récupération des deux individus qui vont se battre 
        S1 = np.random.randint(0, len(P[0,:]), 1)
        P1 = P[:, S1]
        P = np.delete(P, S1, axis=1)
        S2 = np.random.randint(0, len(P[0,:]), 1)
        P2 = P[:, S2]
        P = np.delete(P, S2, axis=1)
        
        # Calcul du meilleur
        distS1 = CalculAdapt(np.resize(P1, (1, len(P1[:, 0])))[0], C)
        distS2 = CalculAdapt(np.resize(P2, (1, len(P2[:, 0])))[0], C)
        
        # On remet en course le gagnant
        if (distS1 < distS2):
            P = np.append(P, P1, axis=1)
        elif(distS2 < distS1):
            P = np.append(P, P2, axis=1)
        else :
            P = np.append(P, P1, axis=1)
            P = np.append(P, P2, axis=1)
    
    return P


# Prend en entrée deux solutions et mélange entre eux une partie de leur chemin
def Croisement(S1, S2) :
    print("Croisement")
    i = 2; j = 3
    
    
     
    
    return newS1, newS2

# 
def Mutation() :
    print("Mutation")
    
def Genetiq() :
    print("Genetique")
    
# =============================================================================
# JEUX DE TESTS
# =============================================================================
#print(f'Carte distance:\n {Carto(6)}')
#print(f'Carte population :\n {Populat()}')
#print(f'Chemin totale: {CalculAdapt()}')
#print(f'Meilleur solution Elite:\n{SelectElit()}')
#print(f'Meilleur solution Tour :\n{SelectTourn()}')











