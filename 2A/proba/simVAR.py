#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 10:26:00 2022

@author: khamard
"""
import random
import math
import matplotlib.pyplot as plt
import numpy as np
import math

# =============================================================================
# EXERCICE 1 :
# =============================================================================
def tirage():
    nb = 0
    proba = random.random()
    
    if ( proba <= 0.25 ) :
        nb = 2
    elif (proba <= 0.375 ) :
        nb = 3
    elif ( proba <= 0.875 ) :
        nb = 4
    elif ( proba <= 1 ) :
        nb = 5
    else : 
        nb = 0

    return nb

#print(f'nb : {tirage()}')
    
def simulation(N = 100) :
    totaux2 = 0
    totaux3 = 0
    totaux4 = 0
    totaux5 = 0
    for i in range(N):
        nb = tirage()
        if (nb == 2):
            totaux2 += 1
        if (nb == 3):
            totaux3 += 1
        if (nb == 4):
            totaux4 += 1
        if (nb == 5):
            totaux5 += 1
            
    return [totaux2/N, totaux3/N, totaux4/N, totaux5/N]
    
#N = 100000
#print(f'Tableau proba {simulation(N)} avec {N} tiages')    
  

# =============================================================================
# EXERCICE 2 : CARRÉ MAGIQUE
# =============================================================================
def VAU(N = 100):
    x = []
    y = []
    
    for i in range (N):
        x.append(random.uniform(-1,1))
        y.append(random.uniform(-1,1))
    
    plt.scatter(x,y)
    plt.title('Nuage de points carré')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show() # Obligatoire pour voir le graphique

#VAU()

# =============================================================================
# EXERCICE 3 : SIMULATION D'UN DÉ
# =============================================================================
def lancerDé():
    rand = random.random()
    
    if (rand <= 1/6):
        nb = 1
    elif (rand <= 2/6):
        nb = 2
    elif (rand <= 3/6):
        nb = 3
    elif (rand <= 4/6):
        nb = 4
    elif (rand <= 5/6):
        nb = 5
    elif (rand <= 6/6):
        nb = 6

    return nb

def simulDé(N = 1000):
    totaux1 = 0
    totaux2 = 0
    totaux3 = 0
    totaux4 = 0
    totaux5 = 0
    totaux6 = 0
    
    for i in range(N):
        nb = lancerDé()
        if (nb == 1):
            totaux1 += 1
        if (nb == 2):
            totaux2 += 1
        if (nb == 3):
            totaux3 += 1
        if (nb == 4):
            totaux4 += 1
        if (nb == 5):
            totaux5 += 1
        if (nb == 6):
            totaux6 += 1
    
    return [totaux1/N, totaux2/N, totaux3/N, totaux4/N, totaux5/N, totaux6/N,]

#print(f'Tableau de probailité pour un dé : {simulDé()}')


# =============================================================================
# EXERCICE 4 : HISTOGRAMME
# =============================================================================
def vecteur(N = 1000):
    vec = np.random.rand(1,N)
    
    return vec

#print(f'vecteur => {vecteur()}')

def histogramme(vec = vecteur(1000)):
    
    plt.hist(vec)
    
    plt.show()
    
#histogramme()   
    
def binomiale(n = vecteur(1000), p=0.5):
    
    plt.hist(n, p)
    plt.show()


binomiale()



# =============================================================================
# LE PARADOXE DU CHEVALIER DE MÉRÉ
# =============================================================================








