var maFenetre ;
function ouvrirFenetre(){
    maFenetre = window.open("", "popUp", "width=500, height=100");
    maFenetre.document.write("<h1>Bienvenu sur la pop-up !</h1>");
    maFenetre.focus(); // Fentre mis au premier plan
}

function deplacerFenetre(){
    maFenetre.moveTo(100, 250);// Fenetre en 100, 250
    //maFenetre.blur(); // Fenetre en arrière plan
    maFenetre.focus();
}

function decalerFenetre(){
    maFenetre.moveBy(200, 0);
    maFenetre.focus();
}

function agrandirFenetre(){
    maFenetre.resizeBy(500, 200);
    maFenetre.focus();
}

function fermerFenetre(){
    maFenetre.close();
}
