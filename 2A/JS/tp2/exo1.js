var choix = 1
while (choix == 1){
    var mot = prompt("Entrer votre mot");
    
    mot = mot.trim() // Permet de supprimer les espaces avant et après
    mot = mot.toLowerCase();
    tailleMot = mot.length; // Permet d'avoir la longueur de la chaine
    l1 = mot.charAt(0); // Donne la première lettre du mot
    mot = mot.replace(l1, 'L'); // Remplace la première lettre par un L || On peut utiliser substring
    mot = mot.concat(l1); // Ajoute à la fin du mot la première lettre
    
    switch (l1) {
        
        case 'b':
            mot = mot.concat('em');
            break ;

        case 'c':
            mot = mot.concat('ès');
            break ;

        case 'd':
            mot = mot.concat('é');
            break ;

        case 'f':
            mot = mot.concat('oque');
            break ;

        case 'm':
            mot = mot.concat('uche');
            break ;

        case 'p':
            mot = mot.concat('é');
            break ;

        default :
            mot = mot.concat('ès');
    }

    document.write(mot); // Affiche le mot

    choix = parseInt(prompt("Voulez-vous continuer || 1- oui, autres : non |"));
}


