function moyTab(tab){
    var somme = 0
    var card = 0 ;
    for (let i = 0 ; i < tab.length ; i++){ // OU   for(let sousTable of tab)
        for (let j = 0 ; j <tab[i].length ; j++){
            somme += tab[i][j]
            card ++ ;
        }
    }
    document.write(`La somme est de ${somme}, et la cardinalité est : ${card}\n`);
    return somme / card ;
}

var monTab = [
    [45,23,54,102,1],
    [40,-4,57,0],
    [38,5,-1]
]

moyenne = moyTab(monTab);

document.write(`La moyenne est de : ${moyenne}`);