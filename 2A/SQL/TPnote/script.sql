drop schema if exists boissons_ctp cascade;
create schema boissons_ctp;
set schema 'boissons_ctp';

-- Tables de base

create table _buveur(
  id      serial      primary key,
  nom     varchar(30) not null,
  prenom  varchar(30) not null
);

INSERT INTO _buveur VALUES (1,'Eastwood','Clint');
INSERT INTO _buveur VALUES (2,'Abitbol','Georges');
INSERT INTO _buveur VALUES (3,'DeNice','Brice');
INSERT INTO _buveur VALUES (4,'Bricot','Juda');
INSERT INTO _buveur VALUES (5,'Meurdesoif','Jean');
INSERT INTO _buveur VALUES (6,'Kollyck','Al');
SELECT setval('_buveur_id_seq',6);

create table _boisson(
  id    serial      primary key,
  nom   varchar(30) not null);
  
create table _biere(
  id    integer     primary key,
  degre decimal     not null,
  type  varchar(20) not null);
alter table _biere add constraint _biere_inherit_fk
  foreign key (id) references _boisson(id);

insert into _boisson values (1,'kronembourg');
insert into _biere values (1,3.5,'pils');
insert into _boisson values (2,'pilsener');
insert into _biere values (2,3,'pils');
insert into _boisson values (3,'heinecken');
insert into _biere values (3,4,'pils');
insert into _boisson values (4,'leffe');
insert into _biere values (4,5.5,'triple');
insert into _boisson values (5,'smirnoff');
insert into _biere values (5,3.5,'arromatisee');
insert into _boisson values (6,'dremwell');
insert into _biere values (6,6,'ale');

create table _jus_fruits(
  id            integer,
  teneur_sucre  decimal not null,
  constraint _jus_fruits_pk primary key (id),
  constraint _jus_fruits_inherit_fk
    foreign key (id) references _boisson(id));

insert into _boisson values (7,'Andros bio Oranges');
insert into _jus_fruits values (7,9.4);
insert into _boisson values (8,'Innocent tutti frutti');
insert into _jus_fruits values (8,10.5);
insert into _boisson values (9,'Tropicana réveil fruité');
insert into _jus_fruits values (9,11);
SELECT setval('_boisson_id_seq',9);

create table _fruit(
  idfruit   serial      primary key,
  nomfruit  varchar(20) not null);
  
insert into _fruit values 
  (1, 'Orange'), (2, 'Mandarine'), (3, 'Raisin blanc'), 
  (4, 'Pomme'),  (5, 'Ananas'), (6,'Carotte'), (7, 'Pêche'),
  (8, 'Poire'), (9, 'Mangue'), (10, 'Citron'), (11, 'Fruit de la passion');
SELECT setval('_fruit_idfruit_seq',11);

create table _composer(
  idjusfruits integer,
  idfruit     integer,
  constraint _composer_pk primary key (idjusfruits, idfruit),
  constraint _composer_fruit_fk
    foreign key (idfruit) references _fruit(idfruit),
  constraint _composer_jusfruits_fk
    foreign key (idjusfruits) references _jus_fruits(id)
);
    
insert into _composer (idjusfruits, idfruit) values
  (7,1),
  (8,1), (8,3), (8,5), (8,6), (8,7), (8,8), (8,9), (8,10), (8,11),
  (9,1), (9,2), (9,3), (9,4);


create table _aimer(
  idbuveur    integer,
  idboisson   integer,
  constraint _aimer_pk primary key (idbuveur,idboisson),
  constraint _aimer_buveur_fk
    foreign key (idbuveur) references _buveur(id),
  constraint _aimer_boisson_fk
    foreign key (idboisson) references _boisson(id));

INSERT INTO _aimer(idbuveur,idboisson)
VALUES  (1,2),(1,3),(1,8), 
        (2,3),(2,7),
        (3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,8),(3,9),
        (4,1),(4,7),(4,8),(4,9),
        (5,7),(5,8),(5,9),
        (6,1),(6,3),(6,5);

-- 1 --
/*
CREATE function f_supprimerBoisson() returns trigger as $$
  BEGIN 
    
    DELETE FROM _aimer where idboisson = old.id ;
    DELETE FROM _biere where id = old.id ;
    
  return null ;
  END ;
$$LANGUAGE plpgsql ;


CREATE trigger t_supprimerBoisson
  before delete
  on _boisson
  for each row 
  execute procedure f_supprimerBoisson() ;

delete from _boisson where id = 1 ;
*/

-- 2 --

create view Jus_Fruits as 
  select * from _jus_fruits natural join _fruit natural join _boisson where _boisson.id = _jus_fruits.id ;

create view Biere as  
  select * from _biere natural join _boisson where _boisson.id = _biere.id ;

create view buveurs_jus_fruits(id_buveur, nom_buveur, id_boisson, nom_boisson, teneur_sucre) as
  select _buveur.id, _buveur.nom, _boisson.id, _boisson.nom, _jus_fruits.teneur_sucre
  from _aimer natural join _buveur natural join _boisson natural join _jus_fruits where _buveur.id = _aimer.idbuveur;



-- 3 --

-- 4 --
create function f_modification_nom_fruit() returns trigger as $$
  BEGIN 
    if (old.nomfruit != new.nomfruit) then
      raise exception 'Interdiction de mofifier le nom d un fruit' ;
    END IF ;
    
  return trigger ;
  END ; 
$$LANGUAGE plpgsql;

create trigger t_modification_nom_fruit
  before update
  on _fruit
  for each row 
  execute procedure f_modification_nom_fruit();

--update _fruit set nomfruit = 'banane' where idfruit = 2 ;

-- 5 --


-- 6 --
/*
  Pour implémenter la multiplicité(1..1) du côté "boisson" dans l'association "aimer", 
  on peut intégrer la clé primaire de _buveur dans _boisson
*/


