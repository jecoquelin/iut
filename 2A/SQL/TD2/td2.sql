----------------
-- Exercice 1 --
----------------

-- 1 --
Un attribut est une clé si elle est unique et non null
Dans notre cas la clé est A 

-- 2 -- 
VAR R BASE RELATION {
    A char,
    B char,
    C char,
} Key {A};

-- 3 --
Transivité : A -> B , B -> C donc A->C

    -- + -- 
    Augmentation : X -> Y donc XA -> YA
    Réflexivité : 

-- 4 --
  -- 1 --
    A -> B ok
    A -> C non
    B -> C ok
    Dans le premier tableau le schéma n''est pas conforme pour B -> C (b1 -> c1 et b1 -> c2) 
  -- 2 --
    A -> B non
    A -> C ok
    B -> C ok
    Dans le seconde tableau le schéma n''est pas conforme pour A -> B (a3 -> b1 et a3 -> b2)
  -- 3 --
    A -> B ok
    A -> C ok
    B -> C ok
    Dans le troisième tableau le schéma est pas conforme


----------------
-- Exercice 2 --
----------------
    A -> C 
    C -> A
    B -> D
    AB -> E (C et D aussi mais pas couverture minimal = Augmentation) 
    AC -> D 
    BC -> E (C aussi mais pas couverture minimal mais ausis redondante avec AB -> E et C->A alors CB -> E )
    

----------------
-- Exercice 3 --
----------------

-- 1 --  
    no_etudiant     -> no_groupe
    no_groupe       -> no_salle, annee_etude
    no_enseignant   -> no_bureau, nom_enseignant
    no_jour, no_tranche_horaire -> no_groupe, no_enseignant, no_matiere


-- 2 --
            no_jour, no_tranche_horaire, no_groupe -> no_enseignant
                    no_etudiant -> no_groupe
    donc    no_jour, no_tranche_horaire, no_etudiant -> no_enseignant

-- 3 --
    La clé primaire est (no_jour, no_tranche_horaire, no_etudiant)


-- 4 -- 
    l''attribut _no_etudiant n''est pas élémentaire car no_groupe permet aussis de trouvé de trouver no_enseigant dans cette clé si on les remplaces 


----------------
-- Exercice 4 --
----------------




