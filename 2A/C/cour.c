/*
    [VARIABLE]
        maVariable  : valeur de la variable
        $maVariable : adresse de la variable
    
    [POINTEURS]
        *monPointeur = NULL ou *monPointeur = &maVariable
        (déclaration et initialisation d'un pointeur)

        monPointeur  : Adresse de la variable pointée
        *monPointeur : valeur de la variable pointée
        &monPointeur : adressse du pointeur
*/

#include <stdio.h>
#include <stdlib.h>

int inverser_nombre(int *pt_nb1, int *pt_nb2){
    int temp = *pt_nb1;
    *pt_nb1 = *pt_nb2 ;
    *pt_nb2 = temp ;
}

int main(){
    int a = 10 ;
    int b = 20 ;
    
    int *pt_a = &a;
    int *pt_b = &b;

    printf("La valeur de a est %d et la valeur de b est %d\n", a, b);
    inverser_nombre(pt_a, pt_b);
    printf("La valeur de a est %d et la valeur de b est %d\n", a, b);
}