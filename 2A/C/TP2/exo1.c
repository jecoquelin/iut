#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Nouveau type pour creer des chaines
typedef char chaine[20] ;

// Création de la structure tPersonne 
typedef struct {
   chaine   nom ;
   int      age ;
} tPersonne ;

// Création de la strucure tElement
typedef struct Elem{
    tPersonne   pers ;
    struct Elem *svt ;
} tElement;

typedef tElement *tliste ;

// Permet d'afficher une personne
void affichagePersonne(tPersonne pp){
    printf("%s %d\n", pp.nom, pp.age);
}

// Permet de parcourir notre liste et afficher les éléments
void affichage(tliste l){
    tElement *aux ;
    aux = l ;
    while (aux != NULL)
    {
        affichagePersonne(aux->pers) ;
        aux = aux->svt ;
    }
}

// Initialise le liste avec la valeur NULL
void init(tliste *l){
    *l = NULL ;
}

// Permet d'insérer un nouveau bloc par le dessus
void isertionEnTete(tliste *l, chaine n, int a){
    // Création d'un nouveau bloc
    tElement *e = (tElement *)malloc(sizeof(tElement));

    // Ajout des valeurs dans ce bloc
    strcpy(e->pers.nom, n);
    e->pers.age = a ;

    // Ajout du bloc à la liste
    e->svt = *l;
    *l = e ;
}

// Permet de rechercher un élement dans la liste
tElement *recherche(tliste l, chaine ch){
    tElement *aux ;

    while ( (aux != NULL) &&  (strcmp(aux->pers.nom, ch)!=0) )
    {
        aux = aux->svt;
    }

    return aux;
}

// Permet de supprimer la tête de la liste
void supprimerTete(tliste *l){
    tElement *p ;
    if ( (*l) != NULL){
        p = (*l) ;
        (*l) = (*l)->svt;
        free(p);
    }
    
}

// Permet de compter le nmobre de personne d'un même nom dans une liste
int compterMemeNom(tliste l, chaine ch){
    tElement *p ;
    int nb = 0 ;
    p = l;
    while (p != NULL){
        if (nbcmp(p->pers.nom, ch) == 0){
            nb++ ;
            l = p->svt;
        }
    }
    return nb ;
}

// Permet de trouver une le tElement au ième élément
tElement *rechercherIeme(tliste l, int ieme){
    tElement *aux = l;
    int i = 1 ;

    while ( (aux != NULL) && (i != ieme ))
    {
         aux = aux->svt;
         i++ ;
    }

    return aux ;
}

// Permet de insérer une personne en fin de liste
void insertionParFin(tliste *l, chaine ch, int a){
    tElement *aux ;
    // Création d'un nouveau bloc
    tElement *e = (tElement *)malloc(sizeof(tElement));

    // Ajout des valeurs dans ce bloc
    strcpy(e->pers.nom, ch);
    e->pers.age = a ;

    e->svt = NULL ;
    if ( (*l)==NULL){
        *l = e ;
    }
    else {
        aux = *l ;
        while (aux->svt != NULL){
            aux = aux->svt ;
        }
        aux->svt = e;
    }
}


// Fonction principal
int main() {
    tliste maliste ;
    chaine nom ;
    int age ;

    init(&maliste);

    printf("Entrer le nom : ");
    scanf("%s", nom);
    printf("Entrer l'age : ");
    scanf("%d", &age);

    while (strcmp(nom, "-1") ){
        isertionEnTete(&maliste, nom, age);
        printf("Entrer le nom : ");
        scanf("%s", nom);
        printf("Entrer l'age : ");
        scanf("%d", &age);
    }
        
    affichage(maliste);

    supprimerTete(maliste);

    return EXIT_SUCCESS;
}