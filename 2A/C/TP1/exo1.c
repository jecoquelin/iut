#include <stdio.h>
#include <stdlib.h>

int main(){
    // Q1
    int x = 100 ;
    int *pt ; 
    pt = &x ;

    *pt = *pt + 10 ;

    printf("pt = %d || x = %d\n", *pt, x);

    // Q2
    pt = (int *)malloc(sizeof(int));
    *pt = 100 ;
    printf("%d\n", *pt);


}