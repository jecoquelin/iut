#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef char chaine20[21];

typedef struct 
    {
        chaine20 nom;
        int population ;
        chaine20 capital ;
    }t_region;

int main(){
    t_region pt;
    
    printf("Donnez son nom");
    scanf("%s", pt.nom);
    printf("Donnez sa population");
    scanf("%d", &(pt.population) );
    printf("Donnez sa capital");
    scanf("%s", pt.capital);

    printf("De tout %s %d %s", pt.nom, pt.population, pt.capital);

    typedef t_region * pt_region;
    pt_region pt1;
    pt1 = (pt_region)malloc(sizeof(t_region));
    strcpy(pt1-> nom, "bretagne");
    pt1->population = 600000;
    strcpy(pt1-> capital, "Rennes");
    printf("%s %d %s", pt1->nom, pt1->population, pt1->capital);
    free(pt1);
}