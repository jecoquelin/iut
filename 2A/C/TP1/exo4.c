#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef char chaine20[21];

typedef struct
{
    chaine20 nom;
    int population;
    int superficie ;
} t_capital;

typedef struct 
{
    chaine20 nom;
    t_capital * capital;
} t_pays;

void init_pays(t_pays * t){
    printf("Saisir la capital du pays:\n");
    scanf("%s", t->nom);
    printf("Entrer la ville");
    scanf("%s", t->capital->nom);
    printf("Entrer la popullation");
    scanf("%d", &(t->capital->population));
    printf("Entrer la superficie");
    scanf("%d", &(t->capital->superficie));
}

void affiche_pays(t_pays t){
    printf("%s", t.nom);
    printf("%s", t.capital->nom);
    printf("%d", t.capital->population);
    printf("%d", t.capital->superficie);
}

int main(){
    t_pays tp;

    init_pays(&tp);
    affiche_pays(tp);
}