#include <stdio.h>

#include <stdlib.h>
#include <time.h>

#define nbElem 1000
#define nbFois 5000
#define parDeux 2500
#define valeurAbsente 100


typedef int tablo[nbElem];


void affTablo(tablo T){
    int i = 0;

    for(i=0; i<nbElem; i++){
       printf("%d ",T[i]);
    }
    printf("\n");
}

int recherche(tablo T, int v){
        int i;

   		i=0;
  		while ((i<nbElem) && (T[i]!= v)) {
			i++;
		}

		if (i==nbElem) return 0;
		else return 1;

}


void tri_selectionCroissant(tablo T){
 int I,J,M;

  for (I=0;I<nbElem;I++){
      for (J=I+1;J<=nbElem;J++){
	      if (T[J]<T[I]) {
	    		M=T[I];
	    		T[I]=T[J];
	    		T[J]=M;
	       }
      }
   }
}

void tri_selectionDecroissant(tablo T){
 int I,J,M;

  for (I=0;I<nbElem;I++){
      for (J=I+1;J<nbElem;J++){
	      if (T[J]>T[I]) {
	    		M=T[I];
	    		T[I]=T[J];
	    		T[J]=M;
	       }
      }
   }
} /* tri_selection */



// Affichage de tablo


// ************************************
// ************************************
// Question 2



int main(){

    tablo tabSelection,tabRecherche;


    clock_t debut,fin;
    float temps;
    int valeur;

    int i = 0;
    int j = 0;


    srand(time(NULL));

    // On contruit  deux tableaux et ils sont affich�s
    // Pour les tests
    // sans la valeur valeurAbsente qui sera pour la recherche au pire des cas

    
    debut = clock();
    for(j=0; j<nbElem; j++){
             valeur=rand();
             if (valeur==valeurAbsente){
                j--;
             }else{
                tabSelection[j] = valeur;
                tabRecherche[j]=tabSelection[j];
             }
        }
    fin = clock();
    temps = ((float)(fin - debut))/CLOCKS_PER_SEC ;

    printf("affichage des tableaux\n");
    affTablo(tabRecherche);
    tri_selectionCroissant(tabSelection);
    affTablo(tabSelection);
    tri_selectionDecroissant(tabSelection);
    affTablo(tabSelection);
    

   printf("Le temps d'execution pour trouver un élément répéter sur %d fois, est de : %f !\n", nbFois, temps);
   

    // le tableau es trie par ordre décroissant
    debut = clock();
    tri_selectionCroissant(tabSelection);
    fin = clock();
    temps = ((float)(fin - debut))/CLOCKS_PER_SEC ;
    printf("Le temps d'execution pour le tri croissant est de : %f !\n", temps);

    // Le tableau est trié par ordre croissant
    debut = clock();
    tri_selectionDecroissant(tabSelection);
    fin = clock();
    temps = ( (float)(fin - debut) )/CLOCKS_PER_SEC ;
    printf("Le temps d'execution pour le tri décroissant est de : %f !\n", temps);
    
    

    

    ////////////////////////////////////////////
    return 0;
}






/* ---------- REPONSE AUX QUESTIONS ---------- */
/*
    -- 1 --
    La fonction recherche est plus rapide car c'est un unique while tendis que la foncion tri_selection est un double for (while)

    -- 2 --
    Pour mesurer le pire cas on doit le trier dans le sens inverse du tri que l'on vas effectuer
    Si on veut voir le tri par selection croissant alors on vas commené à compter lorque notre tableau sera passé avant dans le tris sélection par ordre décroissant

    -- 3 --


    -- 4 --

*/