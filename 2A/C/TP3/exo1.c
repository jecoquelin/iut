// ---------- EXERCICE 1 ---------- //
#define n 1000 

typedef int tablo[n];

int recherche(tablo t, int v){
   int i = 0;
    
    // (k+1)CC + cout(A)+ cout(A)+...
    while ( (i<n) aa (t[i]!= v)){
        i++;
    }

    if (i==n){
        return 0 ;
    }
    else {
        return 1 ;
    }
    
}

//----- Question 1 -----//
/*
    i++     = CA
    while   = (k+1)CC + somme de 1 à k CA = kCC + CC + kCA = k(CA + CC) + CC
    if else = CC

    total = k(CA + CC) + 2CC + CA
*/
//----- Question 2 -----//
/*
    Au meilleur :
    quand k vaut 0      = 2CC + CA

    Au pire :
    quand k vaut 1000   = 1001CA + 1002CC
*/


// ---------- EXERCICE 2 ---------- //

for (i=a;i<=b;i++){
    A;
}
/*
    TRANSFORMATION DU FOR EN WHILE :
        int i = a:
        while (i <= b){
            A ;
            i++ ;
        }
        
    CA + (b - a + 2)CC + somme de a à b (C(Ak) +Caw)
*/



// ---------- EXERCICE 3 ---------- //
/*
    -- 1 --
    (b - a + 2)(CC + CA) + somme de a à b de Cout(Ai) 
    avec a = I+1 ; b = (n-1) Cout(Ai) ; = CC + 3CA
    
    <=> (n-1 -I-1 + 2)(CC + CA) + somme de de I+1 à n-1 de (CC + 3CA)
    <=> (n-I) (CC + CA) + (n-1 - I-1 +1)(CC + 3CA)
    <=> nCC + nCA - ICC - ICA  +  nCC + 3nCA - ICC - 3ICA - CC - 3CA
  P <=> n(2CC + 4CA) + I(-2CC - 4CA) - CC - 3CA


    (n-1 + 2 )(CC + CA) + somme de 0 à n-1 de P
    <=> ((n-1)M) / 2
*/