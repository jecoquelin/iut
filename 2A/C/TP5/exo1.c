#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct Elem{
    int val ;
    struct Elem *svt ;
} tElement ;

typedef struct {
    tElement *tete ;
    tElement *queue ;
    tElement *max ;
    tElement *min ;
} tFileD ;


// Initalise la file
void initialisation(tFileD *f){
    f->tete = NULL ;
    f->queue = NULL ;
    f->max = NULL ;
    f->min = NULL ;
}

// Retourne si la file est vide ou non
// bool estVide(tFileD f){
//     if (f == NULL){
//         return true ;
//     }
//     else {
//         return false ;
//     }
// }

// Permet d'afficher quelques informations sur la file
void afficherInfosFile(tFileD f){
    printf("\n----- File -----\n");
    printf("Tete : %d\tQueue : %d\tMin : %d\tMax : %d\n", f.tete->val, f.queue->val, f.min->val, f.max->val);
}

// Permet d'ajouter un élement à liste et mais à jour min et max si nécessaire
void ajouterEnQueue(tFileD *f, int v){
    tElement *e ;
    e = (tElement *)malloc(sizeof(tElement));
    e->val = v ;
    e->svt = NULL ;
    
    tElement *aux ;
    aux = f->tete ;
    if (aux == NULL){
        f->tete = e ;
        f->queue = e ;
        f->max = e ;
        f->min = e ;
    }
    else {
        f->queue->svt = e ;
        f->queue = f->queue->svt ;

        if (f->min->val > e->val){
            f->min = e ;
        }
        else if (f->max->val < e->val){
            f->max = e ;
        }
    }
}

// Permet de supprimer la tête de liste et met à jour les différente listes
void supprimer(tFileD *f){

}


int main(){
    tFileD maFile ;

    initialisation(&maFile);

    ajouterEnQueue(&maFile, 9);
    ajouterEnQueue(&maFile, -1);
    ajouterEnQueue(&maFile, 90);

    afficherInfosFile(maFile);
}
