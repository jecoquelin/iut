#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef char chaine10[11];

typedef struct {
    int num;
    chaine10 nom ;
    int moyenne ;
}etudiant ;

typedef struct elem{
    etudiant val ;
    struct elem *svt ;
} element ;

typedef element *liste ;



// Permet d'initialiser une liste
void init(liste *l){
    *l = NULL ;
}

// Permet d'ajouter un element à la liste
void ajouter(liste *l, etudiant e ){
    element *aux ;

    // Création d'un bloc
    element *b = (element *)malloc(sizeof(element));
    b->val.num = e.num ;
    strcpy(b->val.nom, e.nom) ;
    b->val.moyenne = e.moyenne ;
    b->svt = NULL ;

    if ( (*l)==NULL){
        *l = b ;
    }
    else {
        aux = *l ;
        while (aux->svt != NULL){
            aux = aux->svt ;
        }
        aux->svt = b;
    }

}

// Affiche un etudiant
void afficherEtudiant(etudiant e){
    printf("--Etudiant--\n");
    printf("Num : %d\tNom : %s\tMoyenne : %d\n", e.num, e.nom, e.moyenne);
}

// Affiche la liste
void affiche(liste l){
    element *aux;
    aux = l ;

    while (aux != NULL){
        afficherEtudiant(aux->val);
        aux = aux->svt ;
    }
}

// Retourne la plus haute moyenne des etudiants présent dans la liste
int max(liste l){
    element *aux ;
    aux = l ;

    int moy = -1 ;
    while (aux != NULL){
        if (moy < aux->val.moyenne){
            moy = aux->val.moyenne ;
        }
        aux = aux->svt ;
    }

    return moy ;
}

// Fonction qui separe les etudiants présent dans la liste l dans deux listes distinctes en fonction de leur moyenne
// Une première, inf10 qui contient la liste des étudiants ayant une note strictement inférieur à 10
// Une seconde, supEga10 qui contient la liste des étudiants ayant une note égale ou supérieure à 10 
void separer(liste l, liste * inf10, liste *supEga10){
    element *aux ;
    aux = l ;

    while (aux !=NULL){
        if (aux->val.moyenne < 10){
            ajouter(inf10, aux->val);
        }
        else {
            ajouter(supEga10, aux->val);
        }
        aux = aux->svt ;
    }

}


// Fonction principal permettant de vérifier le fonctionnement des différentes procédures et fonctions
int main(){
    // Création d'une liste ;
    liste maListe ;


    // Initialistion de la liste
    init(&maListe) ;


    // Création d'étudiant pour remplir la liste    
    etudiant e1 ;
    e1.num = 1 ;
    e1.moyenne = 18;
    strcpy(e1.nom, "Kevin");

    etudiant e2 ;
    e2.num = 2 ;
    e2.moyenne = 2;
    strcpy(e2.nom, "Nathan");

    etudiant e3 ;
    e3.num = 3 ;
    e3.moyenne = 11;
    strcpy(e3.nom, "Mathilde");

    etudiant e4 ;
    e4.num = 4 ;
    e4.moyenne = 14;
    strcpy(e4.nom, "Maude");


    // Ajout des étudiants dans la liste
    ajouter(&maListe, e1);
    ajouter(&maListe, e2);
    ajouter(&maListe, e3);
    ajouter(&maListe, e4);


    // Affiche la liste
    printf("\n----- Affichage de la liste après remplissage -----\n");
    affiche(maListe);


    // Pour trouver la plus haute moyenne présente dans une liste rempli
    printf("\n----- Recherche de la plus haute moyenne avec une liste rempli -----\n");
    int maxMoy ; 
    maxMoy = max(maListe);
    printf("\tLa plus haute moyenne des etudiants present dans la liste est de : %d\n", maxMoy);

    // Pour trouver la plus haute moyenne présente dans une liste vide
    printf("\n----- Recherche de la plus haute moyenne avec une liste vide -----\n");
    liste listeVide ;
    init(&listeVide);
    maxMoy = max(listeVide);
    printf("\tLa plus haute moyenne des etudiants present dans la liste est de : %d\n", maxMoy);


    // Création des deux listes qui contiendra les différents etudiants en fonction de leur moyenne
    liste inf10 ;
    liste supEga10 ;

    // Initialisation des deux listes ;
    init(&inf10);
    init(&supEga10);

    // Distribu les éléments dans liste (maListe) dans les deux autres listes (inf10 et supEga10)
    printf("\n----- Affichage des deux liste après la separation d'une liste en deux listes distinctes en fonction de leur moyenne -----\n");
    separer(maListe, &inf10, &supEga10);

    // Affiche les deux tableaux ;
    printf("-- Liste avec les notes en dessous à 10 --\n");
    affiche(inf10);
    printf("\n--Liste avec les notes égales ou supérieur à 10 --\n");
    affiche(supEga10);

}