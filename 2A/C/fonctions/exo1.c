#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//----- Question 1 -----//
typedef char chaine[51];

typedef struct {
    chaine nom ;
    int age;
}tPersonne;

typedef struct Elem {
    tPersonne pers ;
    struct Elem *svt ;
}tElement ;

typedef tElement *tListe ;

//----- Question 2 -----//
void initListe(tListe *l){
    *l = NULL;
}

//----- Question 3 -----//
void insertionEnTete(tListe *l, chaine ch, int age){
    tElement *e = (tElement *)malloc(sizeof(tElement));
    strcpy(e->pers.nom, ch);
    e->pers.age = age ;

    e->svt = *l ;
    *l = e ;
}

//----- Question 4 -----//
void afficherPersonne(tPersonne p){
    printf("-- Personne --\n");
    printf("Nom : %s\tAge : %d\n", p.nom, p.age);
}

void afficherListe(tListe l){
    tElement *aux ;
    aux = l ;

    while (aux != NULL)
    {
        afficherPersonne(aux->pers);
        aux = aux->svt ;
    }
    
}

//----- Question 5 -----//
tElement *rechercherPersonne(tListe l, chaine ch){
    tElement *aux = l ;

    while ( (aux != NULL) && (strcmp(aux->pers.nom, ch) != 0) ){
        if (strcmp(aux->pers.nom, ch) == 0) {
            return aux ;
        }
        aux = aux->svt ;
    }
    if (aux == NULL){
        return NULL ;
    }
}
//----- Question 6 -----//
void supprimerTete(tListe * l){
    tElement *aux = *l ;

    *l = aux->svt;

    free(aux);
}
//----- Question 7 -----//
void compterPersonne(tListe l, chaine ch){
    int cpt = 0 ;
    tElement *aux = l ;


    while (aux != NULL){
        if (strcmp(aux->pers.nom, ch) == 0){
            cpt = cpt + 1 ;
        }
        aux = aux->svt ;
    }

    printf("Il y a %d personne qui ont le nom %s dans la liste !\n", cpt, ch);
}

//----- Question 8 -----//
tElement *rechercherIeme(tListe l, int i){
    tElement *aux = l;
    int j = 1;

    while ((aux != NULL) && j<i){
        if (j == i){
            return &aux ;
        }

        aux = aux->svt ;
        j++ ;
    }

    if (aux == NULL){
        return NULL ;
    }
}

//----- Question 9 -----//
void insererFin(tListe *l, chaine ch, int age){
    tElement *e = (tElement *)malloc(sizeof(tElement)); 
    strcpy(e->pers.nom, ch);
    e->pers.age = age ;
    e->svt = NULL ;
    
    tElement *aux = *l ;

    if (*l == NULL){
        *l = e ;
    }

    while (aux->svt != NULL){
        aux = aux->svt ;
    }
    aux->svt = e ;
}

//----- Question 10 -----//
void insererApresIeme(tListe *l, chaine ch, int age, int ieme){
    tElement *e = (tElement *)malloc(sizeof(tElement));
    strcpy(e->pers.nom, ch);
    e->pers.age = age ;

    int j = 1 ;
    tElement *aux = *l ;
    while ((aux != NULL) && j!=ieme){
        aux = aux->svt ;
        j++ ;
    }

    if (aux != NULL){
        e->svt = aux->svt ;
        aux->svt = e ;
    }
}

// Fonction principale
int main(){
    tListe l1 ;

    initListe(&l1);

    insertionEnTete(&l1, "Kelian", 18);
    insertionEnTete(&l1, "Loane", 18);
    insertionEnTete(&l1, "Simon", 18);

    afficherListe(l1);

    tElement *adrLoane = rechercherPersonne(l1, "Loane") ;
    printf("L'adresse ou se trouve Loane est : %p\n", &adrLoane);

    supprimerTete(&l1);
    afficherListe(l1);

    compterPersonne(l1, "Simon");
    compterPersonne(l1, "Kelian");

    tElement *adr2i = rechercherIeme(l1, 2);
    printf("L'adresse ou se trouve le 2eme element est : %p\n", adr2i);
    printf("Nom du ieme : %s\n", adr2i->pers.nom);

    insererFin(&l1, "Jeremy", 19);
    afficherListe(l1);

    printf("\n\n---Question 10---\n");
    insererApresIeme(&l1, "Antoine", 22, 2);
    afficherListe(l1);
}