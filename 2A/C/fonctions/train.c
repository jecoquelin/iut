#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define TAILLE 5 

typedef char chaine[51] ;

typedef struct Elem{
    int val ;
    struct Elem *svt ;
}tElement ;

typedef tElement *tListe ;

typedef int tab[TAILLE];


// Initialise le tablo 
void init(tListe *l){
    *l = NULL ;
}

// Ajoute un élément par la tête
void ajouterParTete(tListe *l, int valeur){
    tElement *aux ;
    aux = *l ;

    tElement *p = (tElement *)malloc(sizeof(tElement));
   
    p->val = valeur ;

    p->svt = aux ;
    *l = p ;

}

// Ajoute au seins de la liste chainé les différents éléments d'un tableau par la tête
void ajouterTableauParTete(tListe *l, tab t){

    int i = 0 ;
    while (i < TAILLE){
        ajouterParTete(l, t[i]);
        i++ ;
    }
        
}

// Fonction qui regarde si une valeur est présente dans la liste 
bool appartient(tListe l, int a){
    bool res = false ;
    tElement *aux ;
    aux = l ;
    
    while ( (aux != NULL) && (res != true) ){
        if (aux->val == a){
            printf("coucou");
            res = true ;
        }
        aux = aux->svt ;
    }

    return res ;
}

// Renvoie la cardinalité d'une liste
int cardinalite(tListe l){
    tElement *aux;
    aux = l ;

    int card = 0 ;
    while (aux != NULL){
        card++;
        aux = aux->svt ;
    }

    return card ;
}

// Calcule la moyenne des éléments dans la liste
float moyenne(tListe l){
    float moy = 0 ;
    int total = 0;
    tElement *aux;
    aux = l ;

    while (aux != NULL){
        total = total + aux->val ;
        aux = aux->svt ;
    }

    moy = (float)total / (float)cardinalite(l);
    return moy ;
}

// Affiche la liste chainé
void afficher(tListe l){
    tElement *aux;
    aux = l ;

    while (aux != NULL){
        printf("--Valeur--\n%d\n", aux->val);
        aux = aux->svt ;
    }
    
}

// Trie un tableau par insertion
void tri_insertion(tab t){
    for (int i = 1 ; i <TAILLE;i++){
        int elem = t[i];
        for (int j = i ; (j > 0) && (t[j-1] < elem) ; j--){
            t[j]= t[j-1];
            t[j] = elem ;
        }
    }
}

int main(){
    tListe maListe ;
    tab tablo = {1,2,3,4,5};

    init(&maListe);

    ajouterParTete(&maListe, 2);

    afficher(maListe);

    printf("----- Affichage de la liste apres insertion des elements du tableau dans la liste \n");
    ajouterTableauParTete(&maListe, tablo);
    afficher(maListe);

    bool p2 = appartient(maListe, 8);
    printf("Present : %d\n", p2);

    int card = cardinalite(maListe);
    printf("La cardinalite de la liste est de : %d\n", card);

    float moy = moyenne(maListe);
    printf("La moyenne des elements dans la liste est de : %f\n", moy);
}