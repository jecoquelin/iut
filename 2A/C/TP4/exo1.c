#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


typedef char chaine[50];

typedef struct {
    chaine nom ;
    int age ;
}tPersonne ;

typedef struct Elem{
    tPersonne pers ;
    struct Elem *svt ;
}tElement ;

typedef struct {
    tElement *tete ;
    tElement *queue ;
} tFile ;

typedef struct {
    tFile urgent ;
    tFile important ;
    tFile standard ;
} tFilePrio ;

// Initialisation de la file
void init(tFile *f) {
    f->tete = NULL ;
    f->queue = NULL ;
}

void initPrio(tFilePrio *fp){
    init(&(fp->urgent));
    init(&(fp->important));
    init(&(fp->standard));
}

// Regarde si la file est vide ou non
bool est_vide(tFile f){
    if (f.tete == NULL){
        return true ;
    }
    else {
        return false ;
    }
}

// Permet de récupéré la tête du roi 
tElement *getTete(tFile f){
    return f.tete ;
}

// Permet d'ajouter un élément à la file en connaissant son nom et age
void ajouter(tFile *f, chaine nom, int age){
    // Création d'un bloc
    tElement *e ;
    e = (tElement *)malloc(sizeof(tElement));
    strcpy(e->pers.nom, nom);
    e->pers.age = age ;
    e->svt = NULL ;

    // Ajout du bloc
    if ( est_vide(*f) ){
        f->tete= e ;
        f->tete->svt = f->queue ;
        f->queue = e ;

    }
    else {
        f->queue->svt = e ;
        f->queue = f->queue->svt ;
    }
}

// Permet d'ajouter une personne 
void ajouterP(tFile *f, tPersonne p){
    // Création d'un bloc
    tElement *e ;
    e = (tElement *)malloc(sizeof(tElement));
    strcpy(e->pers.nom, p.nom);
    e->pers.age = p.age ;
    e->svt = NULL ;

    // Ajout du bloc
    if ( est_vide(*f) ){
        f->tete= e ;
        f->tete->svt = f->queue ;
        f->queue = e ;

    }
    else {
        f->queue->svt = e ;
        f->queue = f->queue->svt ;
    }
}

// Permet d'ajouter un élement dans la bonne file en fonction de sa priorité
void ajouterPrio(tFilePrio *fp, tPersonne p, int priorite){

    switch (priorite){
        case 1 :
            ajouterP(&(fp->urgent), p);
        break ;

        case 2 :
            ajouterP(&(fp->important), p);
        break ;

        case 3 :
            ajouterP(&(fp->standard), p);
        break ;

        default :
            printf("Cette priorite n'est pas connu");
    }
}

// Permet de supprimer un élément de la file (tête)
void supprimer(tFile *f){

    if (f->tete == f->queue){
        f->tete = NULL ;
        f->queue = NULL ;
    }
    else {
        f->tete = f->tete->svt ;
    }
}

// Permet d'afficher une file
void afficher(tFile f){
    tElement *aux ;
    aux = f.tete ;
    while (aux != NULL){
        printf("--- Personne ---\n");
        printf("Nom : %s\tAge : %d\n", aux->pers.nom, aux->pers.age);
        aux = aux->svt ;
    }
}

// Permet d'afficher une file avec les différentes priorité
void afficherP(tFilePrio fp){
    if (!est_vide(fp.urgent)){
        printf("\n----- File urgent -----\n");
        afficher(fp.urgent);
        printf("----- File important -----\n");
        afficher(fp.important);
        printf("----- File standard -----\n");
        afficher(fp.standard);
        supprimer(&fp.urgent);
    }
    else if (!est_vide(fp.important)){
        printf("\n----- File urgent -----\n");
        afficher(fp.urgent);
        printf("----- File important -----\n");
        afficher(fp.important);
        printf("----- File standard -----\n");
        afficher(fp.standard);
        supprimer(&fp.important);
    }
    else if (!est_vide(fp.standard)){
        printf("\n----- File urgent -----\n");
        afficher(fp.urgent);
        printf("----- File important -----\n");
        afficher(fp.important);
        printf("----- File standard -----\n");
        afficher(fp.standard);
        supprimer(&fp.standard);
    }
    else {
        printf("\nToutes les listes de priorité sont vide !");
    }
   
}




int main(){
    tFile maFile ;
    tFilePrio mesFiles ;

    init(&maFile);

    bool b = est_vide(maFile);
    printf("%d", b);

    ajouter(&maFile, "Kélian", 18);
    ajouter(&maFile, "Loane", 18);
    ajouter(&maFile, "Jeremy", 19);

    afficher(maFile);



    initPrio(&mesFiles);
    
    tPersonne pers1 ;
    strcpy(pers1.nom, "Jean-Lou");
    pers1.age = 19 ;

    tPersonne pers2 ;
    strcpy(pers2.nom, "Noé");
    pers2.age = 18 ;

    ajouterPrio(&mesFiles, pers1, 1);
    ajouterPrio(&mesFiles, pers2, 2);

    afficherP(mesFiles);

    return EXIT_SUCCESS ;
}